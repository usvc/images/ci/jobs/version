FROM usvc/semver:latest AS source

FROM alpine:3
RUN apk update --no-cache \
  apk upgrade --no-cache
RUN apk add --no-cache \
  git \
  ca-certificates \
  openssh-client
RUN update-ca-certificates
COPY --from=source /semver /bin/semver
COPY ./scripts/after_script /bin/after_script
COPY ./scripts/before_script /bin/before_script
COPY ./scripts/script /bin/script
LABEL SAMPLE_GITLAB_JOB " \
bump: \n\
  only: ["master"] \n\
  stage: versioning \n\
  image: usvc/cijob-version:latest \n\
  before_script: \n\
    - bump-before \n\
  script: \n\
    - bump-script \n\
  after_script: \n\
    - bump-after \n\
"
