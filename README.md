# Version Image for CI Pipelines

[![pipeline status](https://gitlab.com/usvc/images/ci/jobs/version/badges/master/pipeline.svg)](https://gitlab.com/usvc/images/ci/jobs/version/commits/master)
[![dockerhub link](https://img.shields.io/badge/dockerhub-usvc%2Fcijob--version-blue.svg)](https://hub.docker.com/r/usvc/cijob-version)

A CI image for use in `usvc` projects to apply versioning.

# Usage

## GitLab CI

### Standard Usage

Use the image by specifying the `job.image` value as `"usvc/cijob-version:latest"`:

```yaml
job_name:
  stage: stage_name
  image: usvc/cijob-version:latest
  script:
    - ...
```

# Development Runbook

## CI Configuration

Set the following variables in the environment variable configuration of your GitLab CI:

| Key | Value |
| ---: | :--- |
| BASE64_DEPLOY_KEY | Base64-encoded string containing a key we can use to deploy to the repository's `master` branch |
| DOCKER_REGISTRY_URL | URL to your Docker registry |
| DOCKER_REGISTRY_USER | Username for your registry user |
| DOCKER_REGISTRY_PASSWORD | Password for your registry user |
| REPO_HOSTNAME | Hostname for the reposito

# License

Content herein is licensed under the [MIT license](./LICENSE).
