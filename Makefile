# global variables
DATE_TIMESTAMP=$$(date +'%Y')$$(date +'%m')$$(date +'%d')$$(date +'%H')$$(date +'%M')$$(date +'%S')
IMAGE_EXPORT_DIR=./.export
IMAGE_EXPORT_FILE=image.tar
IMAGE_URL=usvc/cijob-version

# override whatever you want in here
-include ./Makefile.properties

image:
	docker build -t $(IMAGE_URL):latest-local .

test: image
	container-structure-test test \
	 	--verbosity debug \
	 	--image $(IMAGE_URL):latest-local \
		--config ./shared/tests/base.yml

publish: image
	mkdir -p ./.make
	printf -- "$(DATE_TIMESTAMP)" > ./.make/publish
	docker tag $(IMAGE_URL):latest-local $(IMAGE_URL):latest
	docker push $(IMAGE_URL):latest
	docker tag $(IMAGE_URL):latest-local $(IMAGE_URL):$$(cat ./.make/publish)
	docker push $(IMAGE_URL):$$(cat ./.make/publish)
	rm -rf ./.make/publish

ci.export:
	mkdir -p $(IMAGE_EXPORT_DIR)
	docker save \
		--output $(IMAGE_EXPORT_DIR)/$(IMAGE_EXPORT_FILE) \
		$(IMAGE_URL):latest-local

ci.import:
	docker load \
		--input $(IMAGE_EXPORT_DIR)/$(IMAGE_EXPORT_FILE)
